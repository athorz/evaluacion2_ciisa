<%-- 
    Document   : salida_delete
    Created on : 26-04-2020, 22:35:36
    Author     : jarqu
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="./bootstrap-4.4.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="./bootstrap-4.4.1/js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Éxito! eliminado - AthorZNet</title>
    </head>
    <body class="bg-dark">
        <div class="container">
            <h2 class="text-center text-white">EXITO!</h2>
            <div class="col-md-12 card border-primary rounded-0">
                <br>
                <h1 class="btn btn-danger btn-block rounded-0 py-2">Equipo Eliminado con éxito! </h1>
                <p> Vuelvo en <span id="countdown">1</span> segundos... </p>
                <%
                    //String redirectURL = "controller_list";
                    //response.sendRedirect(redirectURL);
%>
                <script type="text/javascript">

                    // Total seconds to wait
                    var seconds = 1;

                    function countdown() {
                        seconds = seconds - 1;
                        if (seconds < 0) {
                            // Chnage your redirection link here
                            window.location = "controller_list";
                        } else {
                            // Update remaining seconds
                            document.getElementById("countdown").innerHTML = seconds;
                            // Count down using javascript
                            window.setTimeout("countdown()", 1000);
                        }
                    }

                    // Run countdown function
                    countdown();

                </script>
                <!-- <a href="index.jsp">Volver al Inicio</a>-->
            </div>
        </div>
    </body>
</html>
