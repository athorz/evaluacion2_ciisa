<%-- 
    Document   : agregar
    Created on : 18-04-2020, 16:22:31
    Author     : jarqu
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="./bootstrap-4.4.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="./bootstrap-4.4.1/js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Agregar Nuevo Equipo - AthorZNet</title>
    </head>
    <body class="bg-dark">
        <div class="container">
            <h2 class="text-center text-white">AGREGAR EQUIPO</h2>
            <div class="row justify-content-center">
                <div class="col-12 col-md-8 col-lg-6 pb-5">
                    <form action="controller_add" method="POST">
                        <div class="card border-primary rounded-0">
                            <div class="card-header p-0">
                                <div class="bg-info text-white text-center py-2">
                                    <h3><span class="glyphicon glyphicon-hdd"></span> Datos del Equipo</h3>
                                    <p class="m-0">Escribe los datos correspondientes al equipo físico</p>
                                </div>
                            </div>
                            <!--<h2>Datos del Equipo</h2>-->
                            <div class="card-body p-3">
                                <!--Serie Equipo:<input type="text" name="cod_serie_equipo" value="" /><br>
                                Marca:<input type="text" name="nom_marca" value="" /><br>
                                Modelo:<input type="text" name="nom_modelo" value="" /><br>-->
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">                                        
                                        <div class="input-group-text"><i>Numero Serie</i></div>
                                    </div>
                                    <input type="text" class="form-control" id="cod_serie_equipo" name="cod_serie_equipo" placeholder="Ej.: ABC123456" required>
                                </div>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i>Marca/Fabricante</i></div>
                                    </div>
                                    <input type="text" class="form-control" id="nom_marca" name="nom_marca" placeholder="Ej.: HP, Lenovo, Dell..." required>
                                </div>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i>Modelo</i></div>
                                    </div>
                                    <input type="text" class="form-control" id="nom_modelo" name="nom_modelo" placeholder="Ej.: T480,Precission 8900..." required>
                                </div>

                            </div>
                            <div class="bg-info text-white text-center py-2">
                                <h3><span class="glyphicon glyphicon-user"></span> Datos del Propietario del equipo</h3>
                                <p class="m-0">Escribe los datos correspondientes al dueño asignado del equipo</p>
                            </div>
                            <!--<h2>Datos de Propietario del Equipo</h2>-->

                            <div class="card-body p-3">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i>Nombre</i></div>
                                    </div>
                                    <input type="text" class="form-control" id="nom_propietario" name="nom_propietario" placeholder="Ej.: Juan" required>
                                </div>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i>Apellido Paterno</i></div>
                                    </div>
                                    <input type="text" class="form-control" id="ape1_propietario" name="ape1_propietario" placeholder="Ej.: Perez" required>
                                </div>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i>Apellido Materno</i></div>
                                    </div>
                                    <input type="text" class="form-control" id="ape2_propietario" name="ape2_propietario" placeholder="Ej.:Hernandez" required>
                                </div>
                                <!--Nombre Propietario:<input type="text" name="nom_propietario" value="" /><br>
                                Apellido Paterno Propietario:<input type="text" name="ape1_propietario" value="" /><br>
                                Apellido Materno Propietario:<input type="text" name="ape2_propietario" value="" /><br>-->
                                <!--<input type="text" name="fecha_alta" value="" /><br>-->
                            </div>
                            <div class="text-center">
                                <input class="btn btn-success btn-block rounded-0 py-2" style="font-weight: bold;" type="submit" value="Dar de Alta" />
                            </div>
                            <div ><input class="btn btn-warning btn-block rounded-0 py-2" type="reset" value="Borrar Formulario"></div>
                            <div><a class="col-md-12 btn btn-warning btn-danger rounded-0 py-2" href="controller_list">Volver a la Lista</a></div>
                            <br>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
    <footer><div class="text-light" style="text-align: center; margin-top: 10px;"><p>Programado por : Jorge Arqueros Reyes - Evaluacion 02 CIISA - 18 de Abril del 2020</p></div></footer>
</html>
