<%-- 
    Document   : salida_lista
    Created on : 20-04-2020, 21:57:34
    Author     : jarqu
--%>

<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="cl.athorznet.ev02.model.entities.Equipo"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        --> <link href="./bootstrap-4.4.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <!--<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>-->
        <script src="./bootstrap-4.4.1/js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista de equipos - AthorZNet</title>
    </head>
    <%
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        //SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        List<Equipo> equipos = new ArrayList<>(); //Diamond modificado

        if (request.getAttribute("equipos") != null) {
            equipos = (List<Equipo>) request.getAttribute("equipos");

        }
        Iterator<Equipo> itEquipos = equipos.iterator();
    %> 
    <body class="bg-dark">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12 card border-primary rounded-0">
                    <h1 class="text-center">Equipos registrados en Inventario</h1>
                    <h5 class="text-center">Por favor, antes de elegir una acción, selecciona un equipo</h5><br>
                    <div class="table-responsive">
                        <form name="form_lista" action="controller_edit" method="POST"> 
                            <table class="table table-bordred table-striped">
                                <thead>
                                <th>ID </th>
                                <th>Num. Serie </th>
                                <th>Marca </th>
                                <th>Modelo </th>
                                <th>Nombre Propietario </th>
                                <th>Apellido P. Propietario </th>
                                <th>Apellido M. Propietario </th>
                                <th>Última modif. </th>
                                <!--<th>Editar </th>
                                <th>Borrar </th-->
                                <th>Selec</th>
                                </thead>
                                <tbody>
                                    <%while (itEquipos.hasNext()) {
                                            Equipo equi = itEquipos.next();%>
                                    <tr>
                                        <td><%= equi.getIdRegistro()%></td>
                                        <td><%= equi.getCodSerieEquipo()%></td>
                                        <td><%= equi.getNomMarca()%></td>
                                        <td><%= equi.getNomModelo()%></td>
                                        <td><%= equi.getNomPropietario()%></td>
                                        <td><%= equi.getApe1Propietario()%></td>
                                        <td><%= equi.getApe2Propietario()%></td>
                                        <td><%= dateFormat.format(equi.getFechaAlta())%></td>
                                        <!--<td><p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil"></span></button></p></td>
                                        <td><p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p></td>-->
                                        <td> <input type="radio" name="seleccion" value="<%= equi.getIdRegistro()%>" style = "display: inline-block;vertical-align: top;" required="true"> </td>

                                    </tr>
                                    <%}%>                
                                </tbody>           
                            </table>

                            <br>
                            <div class="text-center col-md-12">
                                <a class="btn btn-lg btn-success" href="agregar.jsp" >Crear</a>
                                <!--<button type="submit"  name="accion"  value="crear" class="btn btn-lg btn-success ">Crear</button>-->

                                <button type="submit" name="accion" value="editar" class="btn btn-lg btn-info " ><span class="glyphicon glyphicon-pencil"></span> Editar</button>

                                <button type="submit" name="accion" value="eliminar" class="btn btn-lg btn-danger"><span class="glyphicon glyphicon-trash"></span> Eliminar </button>
                                <!--<button type="submit"  name="accion"  value="listar" class="btn btn-lg btn-secondary" style="margin-top: 20px;margin-bottom: 50px;">Listar</button>-->
                                <br><br><br>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>

    </body>
    <footer><div class="text-light" style="text-align: center; margin-top: 10px;"><p>Programado por : Jorge Arqueros Reyes - Evaluacion 02 CIISA - 20 de Abril del 2020</p></div></footer>
</html>
