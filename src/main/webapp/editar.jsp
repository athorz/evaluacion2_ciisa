<%-- 
    Document   : editar
    Created on : 21-04-2020, 1:29:10
    Author     : jarqu
--%>

<%@page import="cl.athorznet.ev02.model.entities.Equipo"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="./bootstrap-4.4.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="./bootstrap-4.4.1/js/bootstrap.min.js"></script>
        <title>Cambiar Propietario de Equipo - AthorZNet</title>
    </head>
    <%
        Equipo equipo = (Equipo) request.getAttribute("equipo");

    %>
    <body class="bg-dark">
        <div class="container">
            <h2 class="text-center text-white">CAMBIAR PROPIETARIO DEL EQUIPO</h2>
            <div class="row justify-content-center">
                <div class="col-12 col-md-8 col-lg-6 pb-5">
                    <div class="card border-primary rounded-0">
                        <div class="card-header p-0">
                            <div class="bg-success text-white text-center py-2">
                                <h3><span class="glyphicon glyphicon-hdd"></span> Datos del Equipo</h3>
                                <p class="m-0">Información no modificable</p>
                            </div>
                        </div>
                        <!-- <h3> Datos de equipo (No modificable)</h3>-->
                        <div class="card-body p-3">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i>Num. Serie</i></div>
                                    <!--<label for="num_serie_equipo">Num. Serie</label>-->                              
                                </div>
                                <input type="text" class="form-control" id="serie_dis" name="serie_dis" placeholder=" <%= equipo.getCodSerieEquipo()%>" disabled> 
                            </div>

                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i>Marca/Fabricante</i></div>                         
                                </div>
                                <input type="text" class="form-control" id="marca_dis" name="marca_dis" placeholder=" <%= equipo.getNomMarca()%>" disabled> 
                            </div>       

                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i>Modelo</i></div>                                
                                </div>
                                <input type="text" class="form-control" id="modelo_dis" name="modelo_dis" placeholder=" <%= equipo.getNomModelo()%>" disabled>

                            </div>
                        </div>        
                    </div>

                    <div class="card border-primary rounded-0">
                        <div class="card-header p-0">
                            <div class="bg-info text-white text-center py-2">
                                <h3><span class="glyphicon glyphicon-hdd"></span>Cambiar Propietario</h3>
                                <p class="m-0">Ingrese el nuevo propietario o editar sus datos</p>
                            </div>
                        </div>
                        <!--<h3>Cambiar Propietario (EDITAR)</h3>  --> 
                        <form  name="form" action="controller_graba_edit" method="POST">
                            <input  name="id_registro" type="hidden" value="<%= equipo.getIdRegistro()%>" >
                            <input  name="cod_serie_equipo" type="hidden"  value="<%= equipo.getCodSerieEquipo()%>" >
                            <input  name="nom_marca" type="hidden"  value="<%= equipo.getNomMarca()%>" >
                            <input  name="nom_modelo" type="hidden" value="<%= equipo.getNomModelo()%>" >
                            <input  name="fecha_alta"  type="hidden" value="<%= equipo.getFechaAlta()%>" >

                            <div class="card-body p-3">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">                                        
                                        <div class="input-group-text"><i>Nombre</i></div>
                                    </div>
                                    <input type="text" class="form-control" id="nom_propietario" name="nom_propietario" value="<%= equipo.getNomPropietario()%>" required>
                                </div>
                                <br>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">                                        
                                        <div class="input-group-text"><i>Apellido P.</i></div>
                                    </div>
                                    <input type="text" class="form-control" id="ape1_propietario" name="ape1_propietario" value="<%= equipo.getApe1Propietario()%>" required>
                                </div>

                                <br>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">                                        
                                        <div class="input-group-text"><i>Apellido M.</i></div>
                                    </div>
                                    <input type="text" class="form-control" id="ape2_propietario" name="ape2_propietario" value="<%= equipo.getApe2Propietario()%>" required>
                                </div>

                                <br>
                            </div>
                            <div class="text-center">
                                <!--<input class="btn btn-success btn-block rounded-0 py-2" style="font-weight: bold;" type="submit" value="Dar de Alta" />-->
                                <button type="submit" name="accion" value="grabarEditar" class="btn btn-success btn-block rounded-0 py-2">Grabar</button>
                                <div ><input class="btn btn-warning btn-block rounded-0 py-2" type="reset" value="Borrar Formulario"></div>
                                <div><a class="col-md-12 btn btn-warning btn-danger rounded-0 py-2" href="controller_list">Volver a la Lista</a></div>
                            </div>
                            <br>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body
    <footer><div class="text-light" style="text-align: center; margin-top: 10px;"><p>Programado por : Jorge Arqueros Reyes - Evaluacion 02 CIISA - 21 de Abril del 2020</p></div></footer>
</html>
