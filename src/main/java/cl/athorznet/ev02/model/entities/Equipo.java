/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.athorznet.ev02.model.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jarqu
 */
@Entity
@Table(name = "tb_equipos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Equipo.findAll", query = "SELECT e FROM Equipo e"),
    @NamedQuery(name = "Equipo.findByIdRegistro", query = "SELECT e FROM Equipo e WHERE e.idRegistro = :idRegistro"),
    @NamedQuery(name = "Equipo.findByCodSerieEquipo", query = "SELECT e FROM Equipo e WHERE e.codSerieEquipo = :codSerieEquipo"),
    @NamedQuery(name = "Equipo.findByNomMarca", query = "SELECT e FROM Equipo e WHERE e.nomMarca = :nomMarca"),
    @NamedQuery(name = "Equipo.findByNomModelo", query = "SELECT e FROM Equipo e WHERE e.nomModelo = :nomModelo"),
    @NamedQuery(name = "Equipo.findByNomPropietario", query = "SELECT e FROM Equipo e WHERE e.nomPropietario = :nomPropietario"),
    @NamedQuery(name = "Equipo.findByApe1Propietario", query = "SELECT e FROM Equipo e WHERE e.ape1Propietario = :ape1Propietario"),
    @NamedQuery(name = "Equipo.findByApe2Propietario", query = "SELECT e FROM Equipo e WHERE e.ape2Propietario = :ape2Propietario"),
    @NamedQuery(name = "Equipo.findByFechaAlta", query = "SELECT e FROM Equipo e WHERE e.fechaAlta = :fechaAlta")})
public class Equipo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_registro")
    private Integer idRegistro;
    @Size(max = 2147483647)
    @Column(name = "cod_serie_equipo")
    private String codSerieEquipo;
    @Size(max = 2147483647)
    @Column(name = "nom_marca")
    private String nomMarca;
    @Size(max = 2147483647)
    @Column(name = "nom_modelo")
    private String nomModelo;
    @Size(max = 2147483647)
    @Column(name = "nom_propietario")
    private String nomPropietario;
    @Size(max = 2147483647)
    @Column(name = "ape1_propietario")
    private String ape1Propietario;
    @Size(max = 2147483647)
    @Column(name = "ape2_propietario")
    private String ape2Propietario;
    @Column(name = "fecha_alta")
    @Temporal(TemporalType.DATE)
    private Date fechaAlta;

    public Equipo() {
    }

    public Equipo(Integer idRegistro) {
        this.idRegistro = idRegistro;
    }

    public Integer getIdRegistro() {
        return idRegistro;
    }

    public void setIdRegistro(Integer idRegistro) {
        this.idRegistro = idRegistro;
    }

    public String getCodSerieEquipo() {
        return codSerieEquipo;
    }

    public void setCodSerieEquipo(String codSerieEquipo) {
        this.codSerieEquipo = codSerieEquipo;
    }

    public String getNomMarca() {
        return nomMarca;
    }

    public void setNomMarca(String nomMarca) {
        this.nomMarca = nomMarca;
    }

    public String getNomModelo() {
        return nomModelo;
    }

    public void setNomModelo(String nomModelo) {
        this.nomModelo = nomModelo;
    }

    public String getNomPropietario() {
        return nomPropietario;
    }

    public void setNomPropietario(String nomPropietario) {
        this.nomPropietario = nomPropietario;
    }

    public String getApe1Propietario() {
        return ape1Propietario;
    }

    public void setApe1Propietario(String ape1Propietario) {
        this.ape1Propietario = ape1Propietario;
    }

    public String getApe2Propietario() {
        return ape2Propietario;
    }

    public void setApe2Propietario(String ape2Propietario) {
        this.ape2Propietario = ape2Propietario;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRegistro != null ? idRegistro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Equipo)) {
            return false;
        }
        Equipo other = (Equipo) object;
        if ((this.idRegistro == null && other.idRegistro != null) || (this.idRegistro != null && !this.idRegistro.equals(other.idRegistro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.athorznet.ev02.model.entities.Equipo[ idRegistro=" + idRegistro + " ]";
    }
    
}
