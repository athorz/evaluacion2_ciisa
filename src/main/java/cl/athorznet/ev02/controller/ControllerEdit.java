/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.athorznet.ev02.controller;

import cl.athorznet.ev02.model.dao.EquipoDAO;
import cl.athorznet.ev02.model.dao.exceptions.NonexistentEntityException;
import cl.athorznet.ev02.model.entities.Equipo;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jarqu
 */
@WebServlet(name = "ControllerEdit", urlPatterns = {"/controller_edit"})
public class ControllerEdit extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String seleccion = request.getParameter("seleccion");
        String accion = request.getParameter("accion");
        System.out.println("controllerList  seleccion: " + seleccion);
        System.out.println("controllerList  accion: " + accion);

        if (accion.equalsIgnoreCase("eliminar")) {
            EquipoDAO dao = new EquipoDAO();
            List<Equipo> equipos = new ArrayList<Equipo>();
            try {
                dao.destroy(Integer.parseInt(seleccion));
                equipos = dao.findEquipoEntities();
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(ControllerList.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.setAttribute("equipos", equipos);
            request.getRequestDispatcher("salida_delete.jsp").forward(request, response);
        }

        if (accion.equalsIgnoreCase("crear")) {
            request.getRequestDispatcher("agregar.jsp").forward(request, response);
        }

        if (accion.equalsIgnoreCase("editar")) {

            Equipo equi = new Equipo();
            EquipoDAO dao = new EquipoDAO();

            equi = dao.findEquipo(Integer.parseInt(seleccion));
            request.setAttribute("equipo", equi);
            request.getRequestDispatcher("editar.jsp").forward(request, response);
        }

        
        // processRequest(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
