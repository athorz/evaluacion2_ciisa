/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.athorznet.ev02.controller;

import cl.athorznet.ev02.model.dao.EquipoDAO;
import cl.athorznet.ev02.model.entities.Equipo;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jarqu
 */
@WebServlet(name = "Controller", urlPatterns = {"/controller_add"})
public class ControllerAdd extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String cod_serie_equipo = request.getParameter("cod_serie_equipo");
        String nom_marca = request.getParameter("nom_marca");
        String nom_modelo = request.getParameter("nom_modelo");
        String nom_propietario = request.getParameter("nom_propietario");
        String ape1_propietario = request.getParameter("ape1_propietario");
        String ape2_propietario = request.getParameter("ape2_propietario");
        //Estampa fecha actual
        long millis = System.currentTimeMillis();
        java.sql.Date date = new java.sql.Date(millis);
        
        Equipo equipo = new Equipo();

        equipo.setCodSerieEquipo(cod_serie_equipo);
        equipo.setNomMarca(nom_marca);
        equipo.setNomModelo(nom_modelo);
        equipo.setNomPropietario(nom_propietario);
        equipo.setApe1Propietario(ape1_propietario);
        equipo.setApe2Propietario(ape2_propietario);
        equipo.setFechaAlta(date);

        EquipoDAO dao = new EquipoDAO();
        try {
            dao.create(equipo);
            Logger.getLogger("log").log(Level.INFO, "Valor identificacion de registro del Equipo: {0}", equipo.getIdRegistro());
        } catch (Exception ex) {
            Logger.getLogger("log").log(Level.SEVERE, "Ha ocurrido un Error al intentar INSERTAR un equipo. {0}", ex.getMessage());
        }

        request.getRequestDispatcher("salida_add.jsp").forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
