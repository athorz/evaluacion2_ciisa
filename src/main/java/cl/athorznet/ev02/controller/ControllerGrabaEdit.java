/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.athorznet.ev02.controller;

import cl.athorznet.ev02.model.dao.EquipoDAO;
import cl.athorznet.ev02.model.entities.Equipo;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jarqu
 */
@WebServlet(name = "ControllerGrabaEdit", urlPatterns = {"/controller_graba_edit"})
public class ControllerGrabaEdit extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //String seleccion = request.getParameter("seleccion");
        String accion = request.getParameter("accion");
        if (accion.equalsIgnoreCase("grabarEditar")) {
            try {

                EquipoDAO dao = new EquipoDAO();

                Integer id_registro = Integer.parseInt(request.getParameter("id_registro"));
                String cod_serie_equipo = request.getParameter("cod_serie_equipo");
                String nom_marca = request.getParameter("nom_marca");
                String nom_modelo = request.getParameter("nom_modelo");

                //DateFormat dateFormat = new SimpleDateFormat("E, MMM dd yyyy HH:mm:ss");
               // SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
                String fecha_alta = request.getParameter("fecha_alta");
                request.setAttribute("fecha_alta", fecha_alta);
                
                //java.util.Date fecha1 = formatter1.parse(fecha_alta);
                long millis = System.currentTimeMillis();
                java.sql.Date date = new java.sql.Date(millis);
                
                String nom_propietario = request.getParameter("nom_propietario");
                String ape1_propietario = request.getParameter("ape1_propietario");
                String ape2_propietario = request.getParameter("ape2_propietario");

                Equipo equi = new Equipo();

                equi.setIdRegistro(id_registro);
                equi.setCodSerieEquipo(cod_serie_equipo);
                equi.setNomMarca(nom_marca);
                equi.setNomModelo(nom_modelo);

                equi.setNomPropietario(nom_propietario);
                equi.setApe1Propietario(ape1_propietario);
                equi.setApe2Propietario(ape2_propietario);

                equi.setFechaAlta(date);
                dao.edit(equi);

                List<Equipo> equipos = dao.findEquipoEntities();
                System.out.println("Cantidad Equipos en Base de datos  " + equipos.size());
                System.out.println("Fecha Recibida del request: "+request.getParameter("fecha_alta"));
                request.setAttribute("equipos", equipos);
                request.getRequestDispatcher("salida_lista.jsp").forward(request, response);
            } catch (Exception ex) {
                Logger.getLogger(ControllerEdit.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
